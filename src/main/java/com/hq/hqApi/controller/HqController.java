package com.hq.hqApi.controller;

import com.hq.hqApi.service.HqService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/")
public class HqController {

  @Autowired
  HqService hqService;

  @GetMapping("/cashCardNameById")
  private String getCashCardNameById(@Param(value = "id") Long id) {
    return hqService.getCashCardNameById(id);
  }

  @GetMapping("/cardNameById")
  private String getCardNameById(@Param(value = "id") Long id) {
    return hqService.getCardNameById(id);
  }

  @GetMapping("/bankNameById")
  private String getBankNameById(@Param(value = "id") Long id) {
    return hqService.getBankNameById(id);
  }
}
