package com.hq.hqApi.repository;

import com.hq.hqApi.model.NetBankingBanks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NetBankingBanksRepository extends JpaRepository<NetBankingBanks, Long> {
  NetBankingBanks findFirstById(Long Id);
}
