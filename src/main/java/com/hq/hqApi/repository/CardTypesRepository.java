package com.hq.hqApi.repository;

import com.hq.hqApi.model.CardTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardTypesRepository extends JpaRepository<CardTypes, Long> {
  CardTypes findFirstById(Long id);
}
