package com.hq.hqApi.repository;

import com.hq.hqApi.model.CashCards;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CashCardsRepository extends JpaRepository<CashCards, Long> {
  CashCards findFirstById(Long id);
}
