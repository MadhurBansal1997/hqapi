package com.hq.hqApi.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "net_banking_banks", schema = "hq", catalog = "hq")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NetBankingBanks {
  @Id
  @Column(name = "id")
  Long id;

  @Column(name = "name")
  String name;

}