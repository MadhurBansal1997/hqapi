package com.hq.hqApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HqApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HqApiApplication.class, args);
	}

}
