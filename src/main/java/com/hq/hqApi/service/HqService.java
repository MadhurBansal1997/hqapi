package com.hq.hqApi.service;

public interface HqService {
  String getCashCardNameById(long cashCardId);
  String getCardNameById(long cardId);
  String getBankNameById(long bankId);
}