package com.hq.hqApi.service.impl;

import com.hq.hqApi.model.CardTypes;
import com.hq.hqApi.model.CashCards;
import com.hq.hqApi.model.NetBankingBanks;
import com.hq.hqApi.repository.CardTypesRepository;
import com.hq.hqApi.repository.CashCardsRepository;
import com.hq.hqApi.repository.NetBankingBanksRepository;
import com.hq.hqApi.service.HqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class HqServiceImpl implements HqService {
  @Autowired
  CardTypesRepository cardTypesRepository;

  @Autowired
  CashCardsRepository cashCardsRepository;

  @Autowired
  NetBankingBanksRepository netBankingBanksRepository;

  public String getCashCardNameById(long cashCardId) {
    CashCards cashCards = cashCardsRepository.findFirstById(cashCardId);
    return (cashCards != null && cashCards.getName() != null) ? cashCards.getName() : "No Cash Card Record found for Id:" + cashCardId;
  }

  public String getCardNameById(long cardId) {
    CardTypes cardTypes = cardTypesRepository.findFirstById(cardId);
    return (cardTypes != null && cardTypes.getName() != null) ? cardTypes.getName() : "No Card Type Record found for Id:" + cardId;
  }

  public String getBankNameById(long bankId) {
    NetBankingBanks netBankingBanks = netBankingBanksRepository.findFirstById(bankId);
    return (netBankingBanks != null && netBankingBanks.getName() != null) ? netBankingBanks.getName() : "No Net Banking Banks Record found for Id:" + bankId;
  }
}
